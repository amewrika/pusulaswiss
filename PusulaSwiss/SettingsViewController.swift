//
//  SettingsViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 11/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import OneSignal

class SettingsViewController: BaseViewController {
    @IBOutlet weak var imageHeaderAd: UIImageView!
    @IBOutlet weak var bMenu: UIButton!
    var headerAds: [Advertisement] = []
    var json: JSON?
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        
        let notificationCenter = NotificationCenter.default
      //  notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)

        setMenuButton()
        getHeaderAd()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func appMovedToBackground() {
        print("App moved to background!")
    }
    
    func appMovedToForeground() {
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        if notificationType?.rawValue == 0 {
            self.notificationSwitch.isOn = false
        } else {
            self.notificationSwitch.isOn = true
        }
    }

    
    func getHeaderAd(){
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAdvertisement&ad_type=2", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let ad = Advertisement()
                        ad.ad_id = Int(data["ad_id"].string!)
                        ad.ad_name = data["ad_name"].string
                        
                        let linkURL = data["ad_link"].string
                        ad.ad_link = URL(string: linkURL!)
                        
                        let imageURL = data["ad_image"].string
                        ad.ad_image = URL(string: imageURL!)
                        
                        self.headerAds.append(ad)
                    }
                    
                    let random = Int(arc4random_uniform(UInt32(self.headerAds.count)));
                    self.imageHeaderAd.af_setImage(withURL: self.headerAds[random].ad_image!)
                    self.imageHeaderAd.tag = random
                    
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.tap(gesture:)))
                    self.imageHeaderAd.isUserInteractionEnabled = true
                    self.imageHeaderAd.addGestureRecognizer(tapGestureRecognizer)
                }
        }
    }
    
    @IBAction func openSliderMenu(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    func setMenuButton() {
        let image = self.bMenu.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bMenu.setImage(image, for: .normal)
        self.bMenu.imageView?.tintColor = UIColor.white
        self.bMenu.imageView?.clipsToBounds = true
        self.bMenu.layer.cornerRadius = 24
        self.bMenu.clipsToBounds = true
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        let tappedImage = gesture.view as! UIImageView
        let index = tappedImage.tag
        let url = self.headerAds[index].ad_link
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        let url = NSURL(string: UIApplicationOpenSettingsURLString)! as URL
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        if notificationType?.rawValue == 0 {
            self.notificationSwitch.isOn = false
        } else {
            self.notificationSwitch.isOn = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
