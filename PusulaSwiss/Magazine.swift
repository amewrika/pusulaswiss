//
//  Magazine.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 27/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit

class Magazine: NSObject {
    var id: Int?
    var date: Date?
    var year: String?
    var month: String?
    var title: String?
    var imageURL: URL?
    var pdfLink: URL?
    var type: String?
    var category: Category?
}
