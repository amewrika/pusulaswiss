//
//  Article.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import Foundation

class Category: NSObject {
    var id: Int?
    var name: String?
    var postCount: Int?
}
