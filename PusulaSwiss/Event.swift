//
//  Article.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import Foundation

class Event: NSObject {
    var id: Int?
    var post_date: Date?
    var post_title: String?
    var post_excerpt: String?
    var image: URL?
    var post_content: String?
    var start_date: Date?
    var end_date: Date?
    var location: String?
    var organizer: String?
    var event_type: String?
    var website: URL?
}
