//
//  Article.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import Foundation

class ArticleDetail: Article
{
    //var id: Int?
    var author: String? = ""
    //var date: Date?
    //var title: String? = ""
    var summary: String? = ""
    //var imageURL: URL?
    var url: URL?
    //var isSlider: Bool?
    //var type: String? = ""
    var content: String? = ""
    var post_status: String? = ""
    var comment_count: Int?
    //var category: Category?
    var advertisement: Advertisement?
}
