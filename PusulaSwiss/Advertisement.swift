//
//  Article.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import Foundation

class Advertisement: NSObject {
    var ad_id: Int?
    var ad_name: String?
    var ad_image: URL?
    var ad_link: URL?
}
