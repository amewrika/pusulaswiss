//
//  CategoryViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 26/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class CategoryViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableNews: UITableView!
    
    var json: JSON?
    var articles: [Article] = []
        var headerAds: [Advertisement] = []
    var categoryId = 3
    @IBOutlet weak var imageHeaderAd: UIImageView!
    @IBOutlet weak var bMenu: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = ""
        
        self.tableNews.delegate = self
        self.tableNews.dataSource = self
        
        self.tableNews.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "article")
        
        setMenuButton()
        getArticles()
        getHeaderAd()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setMenuButton() {
        let image = self.bMenu.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bMenu.setImage(image, for: .normal)
        self.bMenu.imageView?.tintColor = UIColor.white
        self.bMenu.imageView?.clipsToBounds = true
        self.bMenu.layer.cornerRadius = 24
        self.bMenu.clipsToBounds = true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "article", for: indexPath) as! ArticleTableViewCell
        cell.articleImage.image = nil
        cell.articleTitle.text = (self.articles[indexPath.section] as Article).title!.uppercased()
        cell.articleTitle.lineBreakMode = .byWordWrapping
        cell.articleTitle.numberOfLines = 0
        cell.articleCategory.text = " " + ((self.articles[indexPath.section] as Article).category?.name?.uppercased())! + " "
        cell.articleCategory.sizeToFit()
        if let imageURL = (self.articles[indexPath.section] as Article).imageURL {
        cell.articleImage.af_setImage(withURL: imageURL)
        }
        
        cell.tag = (self.articles[indexPath.section] as Article).id!
        
        // add border and color
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 0.1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "articleDetailViewController") as! ArticleDetailViewController
        vc.article.id = tableView.cellForRow(at: indexPath)?.tag
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let width = UIScreen.main.bounds.width - 16
        let height = (width * 9 / 16) + 50
        return height
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    @IBAction func openSliderMenu(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    func getHeaderAd(){
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAdvertisement&ad_type=2", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let ad = Advertisement()
                        ad.ad_id = Int(data["ad_id"].string!)
                        ad.ad_name = data["ad_name"].string
                        
                        let linkURL = data["ad_link"].string
                        ad.ad_link = URL(string: linkURL!)
                        
                        let imageURL = data["ad_image"].string
                        ad.ad_image = URL(string: imageURL!)
                        
                        self.headerAds.append(ad)
                    }
                    
                    let random = Int(arc4random_uniform(UInt32(self.headerAds.count)));
                    self.imageHeaderAd.af_setImage(withURL: self.headerAds[random].ad_image!)
                    self.imageHeaderAd.tag = random
                    
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CategoryViewController.tap(gesture:)))
                    self.imageHeaderAd.isUserInteractionEnabled = true
                    self.imageHeaderAd.addGestureRecognizer(tapGestureRecognizer)
                }
        }
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        let tappedImage = gesture.view as! UIImageView
        let index = tappedImage.tag
        let url = self.headerAds[index].ad_link
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    
    func getArticles(){
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getArticlesByCategory&category_id=\(self.categoryId)", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let article = Article()
                        
                        article.id = Int(data["id"].string!)
                        
                        article.title = data["title"].string!
                        
                        if let imageURL = data["imageURL"].string {
                            article.imageURL = URL(string: imageURL)
                        }
                        
                        let date = data["date"].string!
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH-mm-SS"
                        article.date = dateFormatter.date(from: date)
                        
                        article.isSlider = data["isSlider"].boolValue
                        article.category = Category()
                        article.category?.id = Int(data["category"]["id"].string!)
                        article.category?.name = data["category"]["name"].string!.replacingOccurrences(of: "&amp;", with: "&").uppercased()
                        
                        if(article.isSlider == false) {
                            self.articles.append(article)
                        }
                    }
                }
                self.tableNews.reloadData()
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
