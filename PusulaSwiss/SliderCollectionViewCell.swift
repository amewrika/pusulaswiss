//
//  SliderCollectionViewCell.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 01/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit

class SliderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
