//
//  Article.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import Foundation

class Article: NSObject {
    var id: Int?
    var date: Date?
    var title: String?
    var imageURL: URL?
    var isSlider: Bool?
    var type: String?
    var category: Category?
    var ad: Advertisement?
}
