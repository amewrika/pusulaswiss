//
//  ArticleDetailViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import MessageUI
import Alamofire
import SwiftyJSON
import AlamofireImage

class ArticleDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate, MFMailComposeViewControllerDelegate{
    
    var json: JSON?
    var article = ArticleDetail()
    
    @IBOutlet weak var lCategory: UILabel!
    @IBOutlet weak var lTitle: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var contentWebView: UIWebView!
    @IBOutlet weak var lDate: UILabel!
    @IBOutlet weak var lSummary: UILabel!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageAd: UIImageView!
    @IBOutlet weak var imageAdHeight: NSLayoutConstraint!
    @IBOutlet weak var imageAdWidth: NSLayoutConstraint!
    var ads: [Advertisement] = []
    @IBOutlet weak var imageAdTop: NSLayoutConstraint!
    @IBOutlet weak var imageAdBot: NSLayoutConstraint!
    @IBOutlet weak var imageAspectRatio: NSLayoutConstraint!
    @IBOutlet weak var bFacebook: UIButton!
    @IBOutlet weak var bTwitter: UIButton!
    @IBOutlet weak var bWhatsapp: UIButton!
    @IBOutlet weak var bGooglePlus: UIButton!
    @IBOutlet weak var bEmail: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var readMoreTableView: UITableView!
    
    var sliderArticles: [ArticleDetail] = []
    
    var currentIndex = 0
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        readMoreTableView.isHidden = true
        imageAd.isHidden = true
        
        self.activityIndicator.startAnimating()
        
        self.readMoreTableView.delegate = self
        self.readMoreTableView.dataSource = self
        self.readMoreTableView.register(UINib(nibName: "ArticleSmallTableViewCell", bundle: nil), forCellReuseIdentifier: "articleSmall")
    
       
        
        
        if (self.sliderArticles.count == 0) {
            self.pageControl.isHidden = true
            self.getArticle()
        }
        else {
            self.pageControl.numberOfPages = self.sliderArticles.count
            self.pageControl.currentPage = self.currentIndex
            self.addSwipeGestures()
            self.getSliderArticles()
        }
        
        getAd()
        
        lCategory.sizeToFit()
        self.lTitle.lineBreakMode = .byWordWrapping
        self.lTitle.numberOfLines = 0
        self.lSummary.lineBreakMode = .byWordWrapping
        self.lSummary.numberOfLines = 0
        
        self.lCategory.text = ""
        self.lTitle.text = ""
        self.lDate.text = ""
        self.lSummary.text = ""
        
        self.contentWebView.delegate = self
        self.contentWebView.scrollView.isScrollEnabled = false
        self.contentWebView.scrollView.bounces = false
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "articleSmall", for: indexPath) as! ArticleSmallTableViewCell
        /*
        cell.articleImage.image = nil
        if(self.articles.count != 0) {
            cell.articleTitle.text = (self.articles[indexPath.section] as Article).title!
            cell.articleTitle.lineBreakMode = .byWordWrapping
            cell.articleTitle.numberOfLines = 0
            if let imageURL = (self.articles[indexPath.section] as Article).imageURL {
                cell.articleImage.af_setImage(withURL: imageURL)
            }
            cell.tag = (self.articles[indexPath.section] as Article).id!
        }
        */
        // add border and color
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 0.1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func addSwipeGestures(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func loadSliderArticle() {
        self.article = self.sliderArticles[self.currentIndex]
    }
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if(self.currentIndex != 0){
                    self.currentIndex -= 1
                    self.pageControl.currentPage = self.currentIndex
                    self.loadSliderArticle()
                    self.loadArticle()
                    
                    // These values depends on the positioning of your element
                    let right = CGAffineTransform(translationX: 500, y: 0)
                    let center = CGAffineTransform(translationX: 0, y: 0)
    
    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.scrollView.transform = right
                    }, completion: { (true) in
                        self.scrollView.transform = center
                    })
                    
                }
            case UISwipeGestureRecognizerDirection.left:
                if(self.currentIndex != self.sliderArticles.count - 1){
                    self.currentIndex += 1
                    self.pageControl.currentPage = self.currentIndex
                    self.loadSliderArticle()
                    self.loadArticle()
                    
                    let left = CGAffineTransform(translationX: -500, y: 0)
                    let center = CGAffineTransform(translationX: 0, y: 0)
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self.scrollView.transform = left
                    }, completion: { (true) in
                        self.scrollView.transform = center
                    })
                }
            default:
                break
            }
        }
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        let tappedImage = gesture.view as! UIImageView
        let index = tappedImage.tag
        let url = self.ads[index].ad_link
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    
    
    func getAd(){
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAdvertisement&ad_type=1", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let ad = Advertisement()
                        ad.ad_id = Int(data["ad_id"].string!)
                        ad.ad_name = data["ad_name"].string
                        
                        let linkURL = data["ad_link"].string
                        ad.ad_link = URL(string: linkURL!)
                        
                        let imageURL = data["ad_image"].string
                        ad.ad_image = URL(string: imageURL!)
                        
                        self.ads.append(ad)
                    }
                    
                    let random = Int(arc4random_uniform(UInt32(self.ads.count)));
                    self.imageAd.af_setImage(withURL: self.ads[random].ad_image!)
                    self.imageAd.tag = random
                    
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ArticleDetailViewController.tap(gesture:)))
                    self.imageAd.isUserInteractionEnabled = true
                    self.imageAd.addGestureRecognizer(tapGestureRecognizer)
                }
        }
    }
    
    func getSliderArticles(){
        
        for article in self.sliderArticles {
            Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getArticleDetail&article_id=\(article.id!)", method: .get)
                .responseJSON { response in
                    
                    if let value = response.result.value {
                        self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                        let data = self.json!["data"]
                        let isSuccess = self.json!["success"]
                        
                        if(isSuccess == false) {
                            return
                        }

                        article.id = Int(data["id"].string!)
                        article.author = data["author"].string!
                        article.title = data["title"].string!
                        article.summary = data["summary"].string!
                        article.content = data["content_html"].string!
                        
                        if let imageURL = data["imageURL"].string {
                            article.imageURL = URL(string: imageURL)
                        }
                        
                        if let url = data["url"].string {
                            article.url = URL(string: url)
                        }
                        
                        let date = data["date"].string!
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:SS"
                        article.date = dateFormatter.date(from: date)
                        
                        article.isSlider = data["isSlider"].boolValue
                        article.category = Category()
                        article.category?.id = Int(data["category"]["id"].string!)
                        article.category?.name = data["category"]["name"].string!.replacingOccurrences(of: "&amp;", with: "&").uppercased(with: Locale(identifier: "tr"))
                        
                        self.article = self.sliderArticles[self.currentIndex]
                        self.loadArticle()
                    }
            }
        }
    }

    
    func getArticle(){
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getArticleDetail&article_id=\(self.article.id!)", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let data = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS
                    
                    self.article.id = Int(data["id"].string!)
                    self.article.author = data["author"].string!
                    self.article.title = data["title"].string!
                    self.article.summary = data["summary"].string!
                    self.article.content = data["content_html"].string!
                    
                    if let imageURL = data["imageURL"].string {
                        self.article.imageURL = URL(string: imageURL)
                    }
                    if let url = data["url"].string {
                        self.article.url = URL(string: url)
                    }
                    
                    let date = data["date"].string!
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:SS"
                    self.article.date = dateFormatter.date(from: date)
                    
                    self.article.isSlider = data["isSlider"].boolValue
                    self.article.category = Category()
                    self.article.category?.id = Int(data["category"]["id"].string!)
                    self.article.category?.name = data["category"]["name"].string!.replacingOccurrences(of: "&amp;", with: "&").uppercased(with: Locale(identifier: "tr"))
                }
                
                self.loadArticle()
        }
    }
    
    func loadArticle() {
        self.activityIndicator.isHidden = true
        self.articleImage.image = nil
        
        if let category = self.article.category?.name {
            self.lCategory.text = category.uppercased()
        }
        if let title = self.article.title {
            self.lTitle.text = title
        }
        if let summary = self.article.summary {
            self.lSummary.text = summary
        }
        if let content = self.article.content {
            var html = content.replacingOccurrences(of: "700", with: "\(self.view.frame.width)")
            html = html.replacingOccurrences(of: "394", with: "\(self.view.frame.width / 1.77)")
            print(html)
            self.contentWebView.loadHTMLString(html, baseURL: nil)
        }
        if let imageURL = self.article.imageURL {
            self.articleImage.af_setImage(withURL: imageURL)
        }
        if let date = self.article.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateString = dateFormatter.string(from: date)
            
            self.lDate.text = dateString
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    
    @IBAction func shareWithFacebook(_ sender: UIButton) {        
        if let urlString = self.article.url?.absoluteString {
            let shareURLString = "https://www.facebook.com/sharer/sharer.php?u=\(urlString)"
            
            if let shareURL = URL(string: shareURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(shareURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(shareURL)
                }
            }
        }
    }
    @IBAction func shareWithTwitter(_ sender: UIButton) {
        if let urlString = self.article.url?.absoluteString {
            let shareURLString = "https://twitter.com/intent/tweet?text=\(urlString)"
            
            if let shareURL = URL(string: shareURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(shareURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(shareURL)
                }
            }
        }
    }
    @IBAction func shareWithWhatsapp(_ sender: UIButton) {
        if let urlString = self.article.url?.absoluteString {
            if let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
                if let url  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
            }
        }
    }
    @IBAction func shareWithGoogle(_ sender: UIButton) {
        if let urlString = self.article.url?.absoluteString {
            let shareURLString = "https://plus.google.com/share?url=\(urlString)"
            
            if let shareURL = URL(string: shareURLString) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(shareURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(shareURL)
                }
            }
        }
    }
    @IBAction func shareWithEmail(_ sender: UIButton) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            //mail.navigationBar.tintColor = UIColor.white
            mail.setSubject(self.article.title!)

            
            
            mail.navigationItem.title = ""
            //mail.navigationController?.navigationBar.setBackgroundImage(nil, for: <#T##UIBarMetrics#>)
            
            
            
            if let urlString = self.article.url?.absoluteString {
                mail.setMessageBody(urlString, isHTML: true)
                present(mail, animated: true, completion: {
                   // UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
                    
                })
            }

        } else {
            // show failure alert
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
      //  let height = self.contentWebView.stringByEvaluatingJavaScript(from: "document.body.scrollHeight;")
          contentWebView.frame.size.height = 1
        if webView.stringByEvaluatingJavaScript(from: "document.readyState") == "complete" {
        let height = webView.scrollView.contentSize.height
        self.contentWebView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily =\"Dosis-Book\"")
        
       // contentWebView.frame.size.height = 1
       // contentWebView.frame.size = contentWebView.sizeThatFits(.zero)
        
        self.webViewHeight.constant = CGFloat(Int(height) )
        
        webView.setNeedsLayout()
        webView.setNeedsDisplay()
        
        self.bFacebook.isHidden = false
        self.bTwitter.isHidden = false
        self.bWhatsapp.isHidden = false
        self.bGooglePlus.isHidden = false
        self.bEmail.isHidden = false
        self.imageAd.isHidden = false
        

       // self.imageAdBot.constant = 30
        self.imageAdTop.constant = 30
        //self.imageAspectRatio.constant = 0
        self.imageAdWidth.constant = 250
        self.imageAdHeight.constant = 250

        
      //  self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: 60000)
        self.scrollView.setNeedsLayout()
        self.scrollView.setNeedsDisplay()
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        }
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
