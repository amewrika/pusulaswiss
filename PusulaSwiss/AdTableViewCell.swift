//
//  AdTableViewCell.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit

class AdTableViewCell: UITableViewCell {

    @IBOutlet weak var adImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
