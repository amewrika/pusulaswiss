//
//  Article.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import Foundation

class Author: NSObject {
    var id: Int?
    var name: String?
    var imageURL: URL?
    var desc: String?
}
