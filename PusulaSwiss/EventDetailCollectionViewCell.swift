//
//  EventDetailCollectionViewCell.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 07/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit

class EventDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var lType: UILabel!
    @IBOutlet weak var lStartDate: UILabel!
    @IBOutlet weak var lEndDate: UILabel!
    @IBOutlet weak var lLocation: UILabel!
    @IBOutlet weak var lOrganizator: UILabel!
    @IBOutlet weak var lInfo: UILabel!
    @IBOutlet weak var lTitle: UILabel!
}
