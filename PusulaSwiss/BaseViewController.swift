//
//  BaseViewController.swift
//  PusulaSwiss
//
//  Created by icmen on 26.07.2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit


class BaseViewController:UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationController()
        
    }
    
    func setNavigationController(){
        
        //navigationItem.leftBarButtonItem?.tintColor = UIColor.blue
        //navigationItem.backBarButtonItem?.tintColor = UIColor.blue
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.0/255.0, green: 133.0/255.0, blue: 212.0/255.0, alpha: 1.0)
        
        let image = UIImage(named: "pusula_logo_2")//_nav_header
        let titleView = UIImageView(image: image)
        
        self.navigationItem.titleView = titleView
        
        self.navigationController?.navigationItem.titleView?.isHidden = true
        
        self.navigationItem.backBarButtonItem?.title = ""
        
        //UINavigationBar.appearance().backgroundColor = UIColor(red: 2.0/255.0, green: 136.0/255.0, blue: 209.0/255.0, alpha: 1.0)
        //UINavigationBar.appearance().setBackgroundImage(navBackgroundImage, for: .default)
        
        self.setNavigationBarItem()
        
}
}
