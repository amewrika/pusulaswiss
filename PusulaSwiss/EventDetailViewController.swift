//
//  EventDetailViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 07/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

class EventDetailViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var events: [Event] = []
    var currentIndex: Int!
    @IBOutlet weak var collectionEvent: UICollectionView!

    
    var indexPath: IndexPath!
    var json: JSON?
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        self.collectionEvent.delegate = self
        self.collectionEvent.dataSource = self
        
        self.indexPath = IndexPath(row: self.currentIndex, section: 0)
        self.pageControl.numberOfPages = self.events.count
        self.pageControl.currentPage = self.currentIndex
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(EventDetailViewController.swipe(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.collectionEvent.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(EventDetailViewController.swipe(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.collectionEvent.addGestureRecognizer(swipeLeft)
        
        self.collectionEvent.layoutIfNeeded()
        self.collectionEvent.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: false)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func swipe(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if(self.indexPath.row != 0) {
                    self.indexPath = IndexPath(row: indexPath.row - 1, section: 0)
                }
                self.collectionEvent.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
            case UISwipeGestureRecognizerDirection.left:
                if(self.indexPath.row != self.events.count - 1) {
                    self.indexPath = IndexPath(row: indexPath.row + 1, section: 0)
                }
                self.collectionEvent.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
            default:
                break
            }
        }
        
        self.pageControl.currentPage = self.indexPath.row
    }
    
    func getEvents() {
        self.events.removeAll()
        let requestURL = "https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getUpcomingEvents&day=365"
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        Alamofire.request(requestURL, method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let event = Event()
                        //event.id = Int(data["ID"].string!)
                        
                        
                        let start_date = data["start_date"].string
                        let end_date = data["end_date"].string!
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                        
                        event.start_date = dateFormatter.date(from: start_date!)
                        event.end_date = dateFormatter.date(from: end_date)
                        
                        event.post_title = data["post_title"].string
                        event.event_type = data["event_type"].string
                        event.location = data["location"].string
                        event.organizer = data["organizer"].string
                        
                        let imageURL = data["image"].string
                        event.image = URL(string: imageURL!)
                        
                        if let website = data["website"].string {
                            event.website = URL(string: website)
                        }
                        
                        self.events.append(event)
                    }
                }
            
                self.collectionEvent.reloadData()
                self.pageControl.numberOfPages = self.events.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     let width = UIScreen.main.bounds.size.width
     let height =  UIScreen.main.bounds.size.height
     return CGSize(width: width, height: height)
     }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventDetail", for: indexPath) as! EventDetailCollectionViewCell
        cell.lType.text = self.events[indexPath.row].event_type
        
        cell.lLocation.text = self.events[indexPath.row].location
        cell.lOrganizator.text = self.events[indexPath.row].organizer
        cell.lTitle.text = self.events[indexPath.row].post_title
        
        if let URL = self.events[indexPath.row].website {
            cell.lInfo.text = URL.absoluteString
        }
        
        if let date = self.events[indexPath.row].start_date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateString = dateFormatter.string(from: date)
            
            cell.lStartDate.text = dateString
        }
        
        if let date = self.events[indexPath.row].end_date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateString = dateFormatter.string(from: date)
            
            cell.lEndDate.text = dateString
        }
        
        if let imageURL = self.events[indexPath.row].image {
            cell.imageEvent.af_setImage(withURL: imageURL)
        }
        
        return cell
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
