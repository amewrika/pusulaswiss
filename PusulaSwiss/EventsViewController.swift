//
//  EventsViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 11/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage
import SlideMenuControllerSwift

class EventsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var bEvents: UIButton!
    @IBOutlet weak var bLocations: UIButton!
    @IBOutlet weak var selectionTableView: UITableView!
    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var imageHeaderAd: UIImageView!
    @IBOutlet weak var bMenu: UIButton!
    
    var json: JSON?
    var isLocation = false
    var locations: [String] = ["Tüm lokasyonlar"]
    var events: [Event] = []
    var headerAds: [Advertisement] = []
    
    let bgView = UIView(frame: CGRect(x: UIScreen.main.bounds.width , y: UIScreen.main.bounds.height , width: UIScreen.main.bounds.width , height: UIScreen.main.bounds.height))
    var selectedLocation = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.selectionTableView.delegate = self
        self.selectionTableView.dataSource = self
        self.eventsTableView.delegate = self
        self.eventsTableView.dataSource = self
        self.navigationItem.title = ""
        
        self.eventsTableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "event")
        
        
        
        setMenuButton()
        
        let removeBGView: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EventsViewController.removeBGView(_:)))
        removeBGView.numberOfTapsRequired = 1
        self.bgView.addGestureRecognizer(removeBGView)
        
        //  addSingleTapGesture()
        getLocations()
        getUpcomingEvents(day: 365, location: self.selectedLocation)
        getHeaderAd()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setMenuButton() {
        let image = self.bMenu.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bMenu.setImage(image, for: .normal)
        self.bMenu.imageView?.tintColor = UIColor.white
        self.bMenu.imageView?.clipsToBounds = true
        self.bMenu.layer.cornerRadius = 24
        self.bMenu.clipsToBounds = true
    }
    
    @IBAction func bEventsAction(_ sender: UIButton) {
        self.selectionTableView.alpha = 0
        
        if(self.bgView.superview == nil) {
            bgView.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
            bgView.backgroundColor = UIColor.black
            bgView.alpha = 0.0
            self.eventsTableView.addSubview(bgView)
        }
        
        UIView.animate(withDuration: 0.75) {
            self.selectionTableView.isHidden = false
            self.selectionTableView.alpha = 1.0
            self.bgView.alpha = 0.8
        }
        
        self.isLocation = false
        self.selectionTableView.reloadData()
    }
    
    @IBAction func bLocationsAction(_ sender: UIButton) {
        self.selectionTableView.alpha = 0
        
        if(self.bgView.superview == nil) {
            bgView.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2)
            bgView.backgroundColor = UIColor.black
            bgView.alpha = 0.0
            self.eventsTableView.addSubview(bgView)
        }
        
        UIView.animate(withDuration: 0.75) {
            self.selectionTableView.isHidden = false
            self.selectionTableView.alpha = 1.0
            self.bgView.alpha = 0.8
        }
        self.isLocation = true
        self.selectionTableView.reloadData()
    }
    
    func addSingleTapGesture() {
        let singleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EventsViewController.handleSingleTap(_:)))
        singleTap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(singleTap)
    }
    
    func handleSingleTap(_ sender: UITapGestureRecognizer) {
        self.selectionTableView.isHidden = true
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        let tappedImage = gesture.view as! UIImageView
        let index = tappedImage.tag
        let url = self.headerAds[index].ad_link
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    @IBAction func openSliderMenu(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    
    func removeBGView(_ sender: UITapGestureRecognizer)
    {
        removeBGView()
    }
    
    func removeBGView()
    {
        UIView.animate(withDuration: 0.8, animations: {
            self.bgView.alpha = 0.0
            self.selectionTableView.alpha = 0.0
        }, completion: { (true) in
            self.view.endEditing(true)
            self.bgView.removeFromSuperview()
        })
    }
    
    func getHeaderAd(){
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAdvertisement&ad_type=2", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let ad = Advertisement()
                        ad.ad_id = Int(data["ad_id"].string!)
                        ad.ad_name = data["ad_name"].string
                        
                        let linkURL = data["ad_link"].string
                        ad.ad_link = URL(string: linkURL!)
                        
                        let imageURL = data["ad_image"].string
                        ad.ad_image = URL(string: imageURL!)
                        
                        self.headerAds.append(ad)
                    }
                    
                    let random = Int(arc4random_uniform(UInt32(self.headerAds.count)));
                    self.imageHeaderAd.af_setImage(withURL: self.headerAds[random].ad_image!)
                    self.imageHeaderAd.tag = random
                    
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(EventsViewController.tap(gesture:)))
                    self.imageHeaderAd.isUserInteractionEnabled = true
                    self.imageHeaderAd.addGestureRecognizer(tapGestureRecognizer)
                }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == self.selectionTableView) {
            self.selectionTableView.isHidden = true
            var day = 365
            
            removeBGView()
            
            if(self.isLocation == false) {
                let title = tableView.cellForRow(at: indexPath)?.textLabel?.text
                self.bEvents.setTitle(title, for: .normal)
                
                
                switch indexPath.row {
                case 1:
                    day = 7
                    break
                case 2:
                    day = 30
                    break
                case 3:
                    day = 365
                    break
                default:
                    day = 0
                    break
                }
            }
            else {
                self.selectedLocation = (tableView.cellForRow(at: indexPath)?.textLabel?.text)!
                self.bLocations.setTitle(self.selectedLocation, for: .normal)
                if(indexPath.row == 0) {
                    self.selectedLocation = ""
                    self.bLocations.setTitle("Tüm lokasyonlar", for: .normal)
                }
            }
            
            switch day {
            case 0:
                self.getCurrentEvents()
                break
            default:
                self.getUpcomingEvents(day: day, location: self.selectedLocation)
                break
            }
        
        }
        else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "eventDetailViewController") as! EventDetailViewController
            vc.events = self.events
            vc.currentIndex = indexPath.section
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.selectionTableView) {
            
            let cell = selectionTableView.dequeueReusableCell(withIdentifier: "eventSelection", for: indexPath)
            cell.textLabel?.font = UIFont(name: "Dosis-Book", size: 16.0)
            
            if(self.isLocation == false) {
                switch indexPath.row {
                case 0:
                    cell.textLabel?.text = "Devam Eden Etkinlikler"
                    break
                case 1:
                    cell.textLabel?.text = "Bu Hafta Gerçekleşecek Etkinlikler"
                    break
                case 2:
                    cell.textLabel?.text = "Bu Ay Gerçekleşecek Etkinlikler"
                    break
                case 3:
                    cell.textLabel?.text = "Bu Yıl Gerçekleşecek Etkinlikler"
                    break
                default:
                    cell.textLabel?.text = "Devam Eden Etkinlikler"
                    break
                }
            }
            else {
                cell.textLabel?.text = self.locations[indexPath.row]
                cell.textLabel?.font = UIFont(name: "Dosis-Book", size: 16.0)
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "event", for: indexPath) as! EventTableViewCell
            
            if let title = (self.events[indexPath.section] as Event).post_title {
                cell.lTitle.text = title
            }
            if let location = (self.events[indexPath.section] as Event).location {
                cell.lLocation.text = location
            }
            if let start_date = (self.events[indexPath.section] as Event).start_date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:SS"
                let dateString = dateFormatter.string(from: start_date)
                cell.lStartDate.text = "Başlangıç: " + dateString
            }
            if let end_date = (self.events[indexPath.section] as Event).end_date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:SS"
                let dateString = dateFormatter.string(from: end_date)
                
                cell.lEndDate.text = "Bitiş: " + dateString
            }
            if let event_type = (self.events[indexPath.section] as Event).event_type {
                cell.lEventType.text = "Türü: " + event_type
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.selectionTableView) {
            if(self.isLocation == false) {
                return 4
            }
            else {
                return self.locations.count
            }
        }
        else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == self.eventsTableView) {
            return self.events.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(tableView == self.selectionTableView) {
            return "Seçiniz"
        }
        else {
            return ""
        }
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView == self.eventsTableView) {
            return 15
        }
        else {
            return 25
        }
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableView == self.eventsTableView) {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
        }
        else {
            return tableView.headerView(forSection: 0)
        }
    }
    
    func getLocations(){
        
        self.locations = ["Tüm lokasyonlar"]
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getEventLocations", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let name = data["name"].string!
                        self.locations.append(name)
                    }
                }
        }
    }
    
    func getCurrentEvents() {
        self.events.removeAll()
        self.eventsTableView.reloadData()
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getCurrentEvents", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let event = Event()
                        event.id = Int(data["id"].string!)
                        
                        let start_date = data["start_date"].string!
                        let end_date = data["end_date"].string!
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:SS"
                        
                        event.start_date = dateFormatter.date(from: start_date)
                        event.end_date = dateFormatter.date(from: end_date)
                        
                        event.event_type = data["event_type"].string!
                        event.location = data["location"].string!
                        event.organizer = data["organizer"].string!
                        
                        let imageURL = data["image"].string!
                        event.image = URL(string: imageURL)
                        
                        if let website = data["website"].string {
                            event.website = URL(string: website)
                        }
                        self.events.append(event)
                    }
                }
                
                self.eventsTableView.reloadData()
        }
    }
    
    func getUpcomingEvents(day: Int, location: String) {
        self.events.removeAll()
        self.eventsTableView.reloadData()
        var requestURL = ""
        
        if(location != "") {
            requestURL = "https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getUpcomingEvents&day=\(day)&location=\(location)"
        }
        else {
            requestURL = "https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getUpcomingEvents&day=\(day)"
        }
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        Alamofire.request(requestURL, method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let event = Event()
                        //  event.id = Int(data["ID"].string!)
                        
                        
                        let start_date = data["start_date"].string
                        let end_date = data["end_date"].string!
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                        
                        event.start_date = dateFormatter.date(from: start_date!)
                        event.end_date = dateFormatter.date(from: end_date)
                        
                        event.post_title = data["post_title"].string
                        event.event_type = data["event_type"].string
                        event.location = data["location"].string
                        event.organizer = data["organizer"].string
                        
                        let imageURL = data["image"].string
                        event.image = URL(string: imageURL!)
                        
                        if let website = data["website"].string {
                            event.website = URL(string: website)
                        }
                        
                        self.events.append(event)
                    }
                }
                
                self.eventsTableView.reloadData()
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
