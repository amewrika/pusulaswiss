//
//  MagazineViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 25/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SlideMenuControllerSwift

class MagazineViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var bMenu: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var magazine: Magazine!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.delegate = self
        self.activityIndicator.startAnimating()
        
        
        var urlString = "https://docs.google.com/gview?embedded=true&url="
        urlString += (self.magazine.pdfLink?.absoluteString)!
        
        let pdfURL = URL(string: urlString)
        let requestURL = URLRequest(url: pdfURL!)
        self.webView.loadRequest(requestURL)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }
    
    func setMenuButton() {
        let image = self.bMenu.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bMenu.setImage(image, for: .normal)
        self.bMenu.imageView?.tintColor = UIColor.white
        self.bMenu.imageView?.clipsToBounds = true
        self.bMenu.layer.cornerRadius = 24
        self.bMenu.clipsToBounds = true
    }

    @IBAction func openSliderMenu(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
