//
//  AuthorsViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 11/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

class AuthorsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lAuthorName: UILabel!
    @IBOutlet weak var lDescription: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var tableNews: UITableView!
    @IBOutlet weak var imageHeaderAd: UIImageView!
    @IBOutlet weak var bMenu: UIButton!
    
    var authorId = 0
    var json: JSON?
    var articles: [ArticleDetail] = []
        var headerAds: [Advertisement] = []
    var author: Author?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.tableNews.delegate = self
        self.tableNews.dataSource = self
        self.tableNews.register(UINib(nibName: "AuthorArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "authorArticle")
        self.navigationItem.title = ""
        
        setMenuButton()
        getAuthorInfo()
        getArticles()
        getHeaderAd()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setMenuButton() {
        let image = self.bMenu.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bMenu.setImage(image, for: .normal)
        self.bMenu.imageView?.tintColor = UIColor.white
        self.bMenu.imageView?.clipsToBounds = true
        self.bMenu.layer.cornerRadius = 24
        self.bMenu.clipsToBounds = true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "authorArticle", for: indexPath) as! AuthorArticleTableViewCell
        cell.lTitle.text = (self.articles[indexPath.section] as Article).title!
        cell.lTitle.lineBreakMode = .byWordWrapping
        cell.lTitle.numberOfLines = 0
        
        let date = (self.articles[indexPath.section] as Article).date!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let dateString = dateFormatter.string(from: date)
        
        cell.lDate.text = dateString
        
        // add border and color
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 0.1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.selectionStyle = .none
        
        //let imageURL = (self.articles[indexPath.row] as Article).imageURL!
        //cell.articleImage.af_setImage(withURL: imageURL)
        cell.tag = (self.articles[indexPath.section] as Article).id!
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "authorArticleViewController") as! AuthorArticleViewController
        vc.author = self.author
        vc.articles = self.articles
        vc.articleIndex = indexPath.section
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.articles.count
    }
    
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        let tappedImage = gesture.view as! UIImageView
        let index = tappedImage.tag
        let url = self.headerAds[index].ad_link
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    @IBAction func openSliderMenu(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    func getHeaderAd(){
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAdvertisement&ad_type=2", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let ad = Advertisement()
                        ad.ad_id = Int(data["ad_id"].string!)
                        ad.ad_name = data["ad_name"].string
                        
                        let linkURL = data["ad_link"].string
                        ad.ad_link = URL(string: linkURL!)
                        
                        let imageURL = data["ad_image"].string
                        ad.ad_image = URL(string: imageURL!)
                        
                        self.headerAds.append(ad)
                    }
                    
                    let random = Int(arc4random_uniform(UInt32(self.headerAds.count)));
                    self.imageHeaderAd.af_setImage(withURL: self.headerAds[random].ad_image!)
                    self.imageHeaderAd.tag = random
                    
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AuthorsViewController.tap(gesture:)))
                    self.imageHeaderAd.isUserInteractionEnabled = true
                    self.imageHeaderAd.addGestureRecognizer(tapGestureRecognizer)
                }
        }
    }
    
    func getAuthorInfo(){
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAuthors", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let author = Author()
                        
                        author.id = Int(data["id"].string!)
                        author.name = data["name"].string!
                        author.desc = data["description"].string!
                        author.imageURL = URL(string: data["imageURL"].string!)
                        
                        if(self.authorId == author.id) {
                            self.author = author
                        }
                    }
                }
                
                self.lAuthorName.text = self.author?.name
                self.lDescription.text = self.author?.desc
                self.authorImageView.af_setImage(withURL: (self.author?.imageURL!)!)
        }
    }
    
    func getArticles(){
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAuthorArticlesByAuthorId&author_id=\(self.authorId)", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let article = ArticleDetail()
                        
                        article.id = Int(data["id"].string!)
                        
                        article.title = data["title"].string!
                        
                        if let imageURL = data["imageURL"].string {
                            article.imageURL = URL(string: imageURL)
                        }
                        
                        let date = data["date"].string!
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:SS"
                        article.date = dateFormatter.date(from: date)
                        
                        article.isSlider = data["isSlider"].boolValue
                        article.category = Category()
                        
                        self.articles.append(article)
                    }
                }
                self.tableNews.reloadData()
        }
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
