//
//  AuthorArticleTableViewCell.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 18/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit

class AuthorArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var lTitle: UILabel!
    @IBOutlet weak var lDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
