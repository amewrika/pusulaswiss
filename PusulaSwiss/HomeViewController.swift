//
//  ViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import SlideMenuControllerSwift

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var tableNews: UITableView!
    @IBOutlet weak var collectionSliderNews: UICollectionView!
    @IBOutlet weak var tableNewsHeight: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageHeaderAd: UIImageView!
    @IBOutlet weak var bMenu: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bBack: UIButton!
    @IBOutlet weak var bNext: UIButton!
    
    var json: JSON?
    var articles: [ArticleDetail] = []
    var sliderArticles: [ArticleDetail] = []
    var headerAds: [Advertisement] = []
    var ads: [Advertisement] = []
    var refreshControl: UIRefreshControl!
    
    private var indexPath: IndexPath = IndexPath(row: 0, section: 0)
    @IBOutlet weak var pageControl: UIPageControl!
    /*
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    */
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //UINavigationBar.appearance().backgroundColor = UIColor(red: 2.0/255.0, green: 136.0/255.0, blue: 209.0/255.0, alpha: 1.0)
        //UINavigationBar.appearance().setBackgroundImage(navBackgroundImage, for: .default)
        
        
        self.activityIndicator.startAnimating()
        
        self.tableNews.delegate = self
        self.tableNews.dataSource = self
        self.collectionSliderNews.delegate = self
        self.collectionSliderNews.dataSource = self
        
        self.tableNews.register(UINib(nibName: "ArticleSmallTableViewCell", bundle: nil), forCellReuseIdentifier: "articleSmall")
        self.tableNews.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "article")
        self.tableNews.register(UINib(nibName: "AdTableViewCell", bundle: nil), forCellReuseIdentifier: "ad")
        self.collectionSliderNews.register(UINib(nibName: "SliderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "slider")
        
        if let layout = self.collectionSliderNews.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Tekrar yükleniyor")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        self.scrollView.addSubview(refreshControl)
        
        setMenuButton()
        setBackButton()
        setNextButton()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(HomeViewController.swipe(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.collectionSliderNews.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(HomeViewController.swipe(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.collectionSliderNews.addGestureRecognizer(swipeLeft)
        
        getArticles()
        getHeaderAd()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh() {
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.getArticles()
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func setBackButton() {
        let image = self.bBack.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bBack.setImage(image, for: .normal)
        self.bBack.imageView?.tintColor = UIColor.white
        self.bBack.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.1)
    }
 
    func setNextButton() {
        let image = self.bNext.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bNext.setImage(image, for: .normal)
        self.bNext.imageView?.tintColor = UIColor.white
        self.bNext.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.1)
    }
    
    func setMenuButton() {
        let image = self.bMenu.imageView?.image!.withRenderingMode(.alwaysTemplate)
        self.bMenu.setImage(image, for: .normal)
        self.bMenu.imageView?.tintColor = UIColor.white
        self.bMenu.imageView?.clipsToBounds = true
        self.bMenu.layer.cornerRadius = 24
        self.bMenu.clipsToBounds = true
    }

    @IBAction func bBack(_ sender: UIButton) {
        if(self.indexPath.row != 0) {
            self.indexPath = IndexPath(row: indexPath.row - 1, section: 0)
        }
        self.collectionSliderNews.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
        self.pageControl.currentPage = self.indexPath.row
    }
    @IBAction func bNext(_ sender: UIButton) {
        if(self.indexPath.row != self.sliderArticles.count - 1) {
            self.indexPath = IndexPath(row: indexPath.row + 1, section: 0)
        }
        self.collectionSliderNews.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
        self.pageControl.currentPage = self.indexPath.row
    }
    
    func swipe(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if(self.indexPath.row != 0) {
                    self.indexPath = IndexPath(row: indexPath.row - 1, section: 0)
                }
                self.collectionSliderNews.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
            case UISwipeGestureRecognizerDirection.left:
                if(self.indexPath.row != self.sliderArticles.count - 1) {
                    self.indexPath = IndexPath(row: indexPath.row + 1, section: 0)
                }
                self.collectionSliderNews.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
            default:
                break
            }
        }
        
        self.pageControl.currentPage = self.indexPath.row
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        let tappedImage = gesture.view as! UIImageView
        let index = tappedImage.tag
        let url = self.headerAds[index].ad_link
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    
    func squareTap(gesture: UITapGestureRecognizer) {
        let tappedImage = gesture.view as! UIImageView
        let index = tappedImage.tag
        let url = self.articles[index].ad?.ad_link
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    
    func reloadTable() {
        if(self.articles.count > 0 && self.ads.count > 0) {
            self.tableNews.reloadData()
            self.collectionSliderNews.reloadData()

            let screenWidth = Int(UIScreen.main.bounds.width - 16)
            let bigArticleHeight = Int((screenWidth * 9 / 16) + 50)
            
            var articlesHeight = (self.articles.count / 3 + 1) * bigArticleHeight
            articlesHeight += (self.articles.count / 2) * 75
            let adsHeight = (self.articles.count / 9) * screenWidth
            
            let spaceHeight = (self.tableNews.numberOfSections - 1) * 15
            
            let calculatedHeight = articlesHeight + adsHeight + spaceHeight
            self.tableNewsHeight.constant = CGFloat(calculatedHeight)
        }
    }
    
    func getHeaderAd(){
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAdvertisement&ad_type=2", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let ad = Advertisement()
                        ad.ad_id = Int(data["ad_id"].string!)
                        ad.ad_name = data["ad_name"].string
                        
                        let linkURL = data["ad_link"].string
                        ad.ad_link = URL(string: linkURL!)
                        
                        let imageURL = data["ad_image"].string
                        ad.ad_image = URL(string: imageURL!)
                        
                        self.headerAds.append(ad)
                    }
                    
                    let random = Int(arc4random_uniform(UInt32(self.headerAds.count)));
                    self.imageHeaderAd.af_setImage(withURL: self.headerAds[random].ad_image!)
                    self.imageHeaderAd.tag = random
                    
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.tap(gesture:)))
                    self.imageHeaderAd.isUserInteractionEnabled = true
                    self.imageHeaderAd.addGestureRecognizer(tapGestureRecognizer)
                }
        }
    }
    
    func getAd(){
        Alamofire.request("https://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAdvertisement&ad_type=1", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let ad = Advertisement()
                        ad.ad_id = Int(data["ad_id"].string!)
                        ad.ad_name = data["ad_name"].string
                        
                        let linkURL = data["ad_link"].string
                        ad.ad_link = URL(string: linkURL!)
                        
                        let imageURL = data["ad_image"].string
                        ad.ad_image = URL(string: imageURL!)
                        
                        self.ads.append(ad)
                    }
                    
                    for i in 0...self.articles.count - 1 {
                        let random = Int(arc4random_uniform(UInt32(self.ads.count)));
                        if((i+1) % 10 == 0) {
                            let article = ArticleDetail()
                            article.ad = Advertisement()
                            article.ad = self.ads[random]
                            
                            self.articles.insert(article, at: i)
                        }
                    }

                    
                    self.reloadTable()
                }
        }
    }
    
    func getArticles(){
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        
        self.articles.removeAll()
        self.sliderArticles.removeAll()
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getSliderArticles", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let article = ArticleDetail()
                        
                        article.id = Int(data["id"].string!)
                        
                        article.title = data["title"].string!
                        
                        if let imageURL = data["imageURL"].string {
                            article.imageURL = URL(string: imageURL)
                        }
                        
                        let date = data["date"].string!
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH-mm-SS"
                        article.date = dateFormatter.date(from: date)
                        
                        article.isSlider = data["isSlider"].boolValue
                        article.category = Category()
                        article.category?.id = Int(data["category"]["id"].string!)
                        article.category?.name = data["category"]["name"].string!.replacingOccurrences(of: "&amp;", with: "&").uppercased(with: Locale(identifier: "tr"))
                        
                        if(article.isSlider == false) {
                            self.articles.append(article)
                        }
                        else {
                            self.sliderArticles.append(article)
                        }
                    }
                }
                
                self.activityIndicator.isHidden = true
                self.getAd()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.size.width
        let height =  width * 0.545
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "articleDetailViewController") as! ArticleDetailViewController
        vc.article.id = collectionView.cellForItem(at: indexPath)?.tag
        vc.currentIndex = indexPath.row
        vc.sliderArticles = self.sliderArticles
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sliderArticles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "slider", for: indexPath) as! SliderCollectionViewCell
        cell.lTitle.text = self.sliderArticles[indexPath.row].title!
        cell.lTitle.lineBreakMode = .byWordWrapping
        cell.lTitle.numberOfLines = 0
        cell.tag = self.sliderArticles[indexPath.row].id!
        let imageURL = (self.sliderArticles[indexPath.row] as Article).imageURL!
        cell.image.af_setImage(withURL: imageURL)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section != 0 && (indexPath.section + 1) % 10 == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ad", for: indexPath) as! AdTableViewCell
            cell.adImage.image = nil


            if let imageURL = self.articles[indexPath.section].ad?.ad_image {
                cell.adImage.af_setImage(withURL: imageURL)
                cell.adImage.tag = indexPath.section
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.squareTap(gesture:)))
                cell.adImage.isUserInteractionEnabled = true
                cell.adImage.addGestureRecognizer(tapGestureRecognizer)
            }
            return cell
        }
        else if((indexPath.section - (indexPath.section + 1) / 10) % 3 == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "article", for: indexPath) as! ArticleTableViewCell
            cell.articleImage.image = nil
            if(self.articles.count != 0) {
            cell.articleTitle.text = (self.articles[indexPath.section] as Article).title!.uppercased(with: Locale(identifier: "tr"))
            cell.articleTitle.lineBreakMode = .byWordWrapping
            cell.articleTitle.numberOfLines = 0
            cell.articleCategory.text = " " + ((self.articles[indexPath.section] as Article).category?.name?.uppercased(with: Locale(identifier: "tr")))! + " "
            cell.articleCategory.sizeToFit()
            if let imageURL = (self.articles[indexPath.section] as Article).imageURL{
                cell.articleImage.af_setImage(withURL: imageURL)
            }
            cell.tag = (self.articles[indexPath.section] as Article).id!
            }
            // add border and color
            cell.backgroundColor = UIColor.white
            cell.layer.borderColor = UIColor.black.cgColor
            cell.layer.borderWidth = 0.1
            cell.layer.cornerRadius = 8
            cell.clipsToBounds = true
            cell.selectionStyle = .none
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "articleSmall", for: indexPath) as! ArticleSmallTableViewCell
            cell.articleImage.image = nil
            if(self.articles.count != 0) {
            cell.articleTitle.text = (self.articles[indexPath.section] as Article).title!
            cell.articleTitle.lineBreakMode = .byWordWrapping
            cell.articleTitle.numberOfLines = 0
            if let imageURL = (self.articles[indexPath.section] as Article).imageURL {
                cell.articleImage.af_setImage(withURL: imageURL)
            }
            cell.tag = (self.articles[indexPath.section] as Article).id!
            }
            
            // add border and color
            cell.backgroundColor = UIColor.white
            cell.layer.borderColor = UIColor.black.cgColor
            cell.layer.borderWidth = 0.1
            cell.layer.cornerRadius = 8
            cell.clipsToBounds = true
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  if() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "articleDetailViewController") as! ArticleDetailViewController
            vc.article.id = tableView.cellForRow(at: indexPath)?.tag
            self.navigationController?.pushViewController(vc, animated: true)
      //  }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section != 0 && (indexPath.section + 1) % 10 == 0) {
            let width = UIScreen.main.bounds.width - 16
            let height = width
            return height
        }
        else if((indexPath.section - (indexPath.section + 1) / 10)  % 3 == 0) {
            let width = UIScreen.main.bounds.width - 16
            let height = (width * 9 / 16) + 50
            return height
        }
        else {
            return 75
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let count = self.articles.count
        return count
    }
    
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    @IBAction func openSliderMenu(_ sender: UIButton) {
        self.slideMenuController()?.openLeft()
    }
}

extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

