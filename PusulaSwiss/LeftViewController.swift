//
//  MenuViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 26/03/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SlideMenuControllerSwift

class LeftViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var mainViewController: UIViewController!
    var eventsViewController: UIViewController!
    var settingsViewController: UIViewController!
    var authorsViewController: UIViewController!
    var homeViewController: UIViewController!
    var categoryViewController: UIViewController!
    var magazineViewController: UIViewController!
    
    @IBOutlet weak var menuTableView: UITableView!
    
    @IBOutlet weak var bNews: UIButton!
    @IBOutlet weak var bAuthors: UIButton!
    @IBOutlet weak var bEvents: UIButton!
    @IBOutlet weak var bMagazine: UIButton!
    @IBOutlet weak var bSettings: UIButton!

    
    var json: JSON?
    var categories: [Category] = []
    var authors: [Author] = []
    var magazines: [Magazine] = []

    var menuIndex = Menu.news
    
    enum Menu {
        case news
        case authors
        case events
        case magazine
        case settings
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        
        let eventsViewController = storyboard?.instantiateViewController(withIdentifier: "eventsViewController") as! EventsViewController
        self.eventsViewController = UINavigationController(rootViewController: eventsViewController)
        
        let settingsViewController = storyboard?.instantiateViewController(withIdentifier: "settingsViewController") as! SettingsViewController
        self.settingsViewController = UINavigationController(rootViewController: settingsViewController)
        
        let magazineViewController = storyboard?.instantiateViewController(withIdentifier: "magazineViewController") as! MagazineViewController
        self.magazineViewController = UINavigationController(rootViewController: magazineViewController)
        
        getCategories()
        
        setButtonViews()
        bNews.backgroundColor = UIColor(red: 2/255, green: 136/255, blue: 209/255, alpha: 1.0)
        bNews.layer.borderWidth = 0

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setButtonViews() {
        bNews.layer.borderColor = UIColor.white.cgColor
        bNews.layer.borderWidth = 2
        bNews.layer.cornerRadius = bNews.frame.size.width / 2
        bNews.backgroundColor = UIColor(red: 67/255, green: 87/255, blue: 110/255, alpha: 1.0)
        bNews.clipsToBounds = true
        
        bAuthors.layer.borderColor = UIColor.white.cgColor
        bAuthors.layer.borderWidth = 2
        bAuthors.layer.cornerRadius = bNews.frame.size.width / 2
        bAuthors.backgroundColor = UIColor(red: 67/255, green: 87/255, blue: 110/255, alpha: 1.0)
        bAuthors.clipsToBounds = true
        
        bEvents.layer.borderColor = UIColor.white.cgColor
        bEvents.layer.borderWidth = 2
        bEvents.layer.cornerRadius = bNews.frame.size.width / 2
        bEvents.backgroundColor = UIColor(red: 67/255, green: 87/255, blue: 110/255, alpha: 1.0)
        bEvents.clipsToBounds = true
        
        bMagazine.layer.borderColor = UIColor.white.cgColor
        bMagazine.layer.borderWidth = 2
        bMagazine.layer.cornerRadius = bNews.frame.size.width / 2
        bMagazine.backgroundColor = UIColor(red: 67/255, green: 87/255, blue: 110/255, alpha: 1.0)
        bMagazine.clipsToBounds = true
        
        bSettings.layer.borderColor = UIColor.white.cgColor
        bSettings.layer.borderWidth = 2
        bSettings.layer.cornerRadius = bNews.frame.size.width / 2
        bSettings.backgroundColor = UIColor(red: 67/255, green: 87/255, blue: 110/255, alpha: 1.0)
        bSettings.clipsToBounds = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.menuIndex {
        case Menu.news:
            return self.categories.count
        case Menu.authors:
            return self.authors.count
        case Menu.magazine:
            return self.magazines.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch self.menuIndex {
        case Menu.news:
            if(indexPath.row == 0) {
                let homeViewController = storyboard?.instantiateViewController(withIdentifier: "homeViewController") as! HomeViewController
                self.homeViewController = UINavigationController(rootViewController: homeViewController)
                self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
            }
            else {
                let categoryId = self.categories[indexPath.row].id
                let categoryViewController = storyboard?.instantiateViewController(withIdentifier: "categoryViewController") as! CategoryViewController
                categoryViewController.categoryId = categoryId!
                self.categoryViewController = UINavigationController(rootViewController: categoryViewController)
                self.slideMenuController()?.changeMainViewController(self.categoryViewController, close: true)
                
            }
        case Menu.authors:
            let authorId = self.authors[indexPath.row].id
            let authorsViewController = storyboard?.instantiateViewController(withIdentifier: "authorsViewController") as! AuthorsViewController
            authorsViewController.authorId = authorId!
            self.authorsViewController = UINavigationController(rootViewController: authorsViewController)
            self.slideMenuController()?.changeMainViewController(self.authorsViewController, close: true)
        case Menu.magazine:
            let magazineViewController = storyboard?.instantiateViewController(withIdentifier: "magazineViewController") as! MagazineViewController
            magazineViewController.magazine = self.magazines[indexPath.row]
            self.magazineViewController = UINavigationController(rootViewController: magazineViewController)
            self.slideMenuController()?.changeMainViewController(self.magazineViewController, close: true)
            
        default:
            return
        }
 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menu", for: indexPath)
        
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.font = UIFont(name: "Dosis-Bold", size: 14.0)

        
        switch self.menuIndex {
        case Menu.news:
            cell.textLabel?.text = self.categories[indexPath.row].name
        case Menu.authors:
            cell.textLabel?.text = self.authors[indexPath.row].name
        case Menu.magazine:
            cell.textLabel?.text = self.magazines[indexPath.row].title
        default:
            return cell
        }
        
        cell.textLabel?.text = cell.textLabel?.text?.replacingOccurrences(of: "&amp;", with: "&").uppercased(with: Locale(identifier: "tr"))
        
        return cell
    }

    @IBAction func news(_ sender: UIButton) {
        setButtonViews()
        sender.backgroundColor = UIColor(red: 2/255, green: 136/255, blue: 209/255, alpha: 1.0)
        sender.layer.borderWidth = 0
        self.menuIndex = Menu.news
        getCategories()
    }
    @IBAction func authors(_ sender: UIButton) {
        setButtonViews()
        sender.backgroundColor = UIColor(red: 2/255, green: 136/255, blue: 209/255, alpha: 1.0)
        sender.layer.borderWidth = 0
        self.menuIndex = Menu.authors
        getAuthors()
    }
    @IBAction func events(_ sender: UIButton) {
        self.menuIndex = Menu.events
        self.menuTableView.reloadData()
        setButtonViews()
        sender.backgroundColor = UIColor(red: 2/255, green: 136/255, blue: 209/255, alpha: 1.0)
        sender.layer.borderWidth = 0
        
        self.slideMenuController()?.changeMainViewController(self.eventsViewController, close: true)
    }
    @IBAction func eMagazine(_ sender: UIButton) {
        self.menuIndex = Menu.magazine
        setButtonViews()
        sender.backgroundColor = UIColor(red: 2/255, green: 136/255, blue: 209/255, alpha: 1.0)
        sender.layer.borderWidth = 0
        getMagazines()
    }
    @IBAction func settings(_ sender: UIButton) {
        self.menuIndex = Menu.settings
        self.menuTableView.reloadData()
        setButtonViews()
        sender.backgroundColor = UIColor(red: 2/255, green: 136/255, blue: 209/255, alpha: 1.0)
        sender.layer.borderWidth = 0
        self.slideMenuController()?.changeMainViewController(self.settingsViewController, close: true)
    }
    
    func getCategories() {
        
        self.categories.removeAll()
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAllCategories", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let category = Category()
                        
                        category.id = Int(data["id"].string!)
                        category.name = data["name"].string!
                        
                        if(category.id == 1370) {
                            continue
                        }
                        if(category.id == 739){
                            self.categories.insert(category, at: 5)
                            continue
                        }
                        if(category.id == 1415){
                            self.categories.insert(category, at: 6)
                            continue
                        }
                        self.categories.append(category)
                    }
                }
                
                self.menuTableView.reloadData()
        }
    }

    func getAuthors() {
        
        self.authors.removeAll()
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getAuthors", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let jsonData = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    
                    // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                    
                    for data in jsonData.array! {
                        let author = Author()
                        
                        author.id = Int(data["id"].string!)
                        author.name = data["name"].string!
                        
                        self.authors.append(author)
                    }
                }
                
                self.menuTableView.reloadData()
        }
    }
    
    func getMagazines() {
        
        self.magazines.removeAll()
        
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getMagazines", method: .get)
        .responseJSON { response in
            
            if let value = response.result.value {
                self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                let jsonData = self.json!["data"]
                let isSuccess = self.json!["success"]
                
                if(isSuccess == false) {
                    return
                }
                
                // TO STORE JSON DATA LOCALLY IN OUR CLASS -> ARRAY
                
                for data in jsonData.array! {
                    let magazine = Magazine()
                    
                    magazine.id = Int(data["id"].string!)
                    magazine.title = data["title"].string!
                    
                    let pdfURL = data["pdfLink"].string
                    magazine.pdfLink = URL(string: pdfURL!)
                    
                    self.magazines.append(magazine)
                }
            }
            
            self.menuTableView.reloadData()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
