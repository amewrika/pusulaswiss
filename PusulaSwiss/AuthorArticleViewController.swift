//
//  AuthorArticleViewController.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

class AuthorArticleViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIWebViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var articles: [ArticleDetail]!
    var articleIndex: Int!
    var author: Author!
    
    var indexPath: IndexPath!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var json: JSON?
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.indexPath = IndexPath(row: self.articleIndex, section: 0)

        self.pageControl.numberOfPages = self.articles.count
        self.pageControl.currentPage = self.indexPath.row
        getArticleDetails()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(AuthorArticleViewController.swipe(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.collectionView.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(AuthorArticleViewController.swipe(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.collectionView.addGestureRecognizer(swipeLeft)
        
        self.collectionView.layoutIfNeeded()
        self.collectionView.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: false)

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func swipe(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if(self.indexPath.row != 0) {
                    self.indexPath = IndexPath(row: indexPath.row - 1, section: 0)
                }
                self.collectionView.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
            case UISwipeGestureRecognizerDirection.left:
                if(self.indexPath.row != self.articles.count - 1) {
                    self.indexPath = IndexPath(row: indexPath.row + 1, section: 0)
                }
                self.collectionView.scrollToItem(at: self.indexPath, at: .centeredHorizontally, animated: true)
            default:
                break
            }
        }
        
        self.pageControl.currentPage = self.indexPath.row
    }
    
    func getArticleDetails(){
        
        // WE USED "ALAMOFIRE" (AFNETWORK) and "SWIFTYJSON" TO PARSE JSON DATA.
        for article in articles {
        Alamofire.request("http://www.pusulaswiss.ch/services/admin/index.php?route=services/news/getArticleDetail&article_id=\(article.id!)", method: .get)
            .responseJSON { response in
                
                if let value = response.result.value {
                    self.json = JSON(value) // TO WRITE DATA OUR "SWIFTYJSON" OBJECT
                    let data = self.json!["data"]
                    let isSuccess = self.json!["success"]
                    
                    if(isSuccess == false) {
                        return
                    }
                    

                    article.id = Int(data["id"].string!)
                    article.author = data["author"].string!
                    article.title = data["title"].string!
                    article.summary = data["summary"].string!
                    article.content = data["content_html"].string!
                    
                    if let imageURL = data["imageURL"].string {
                        article.imageURL = URL(string: imageURL)
                    }
                    
                    let date = data["date"].string!
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:SS"
                    article.date = dateFormatter.date(from: date)
                    
                    article.isSlider = data["isSlider"].boolValue
                    article.category = Category()
                    article.category?.id = Int(data["category"]["id"].string!)
                    article.category?.name = data["category"]["name"].string!.replacingOccurrences(of: "&amp;", with: "&").uppercased(with: Locale(identifier: "tr"))
                    
                }
                
                self.collectionView.reloadData()
        }
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.size.width
        let height =  UIScreen.main.bounds.size.height
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "authorArticle", for: indexPath) as! AuthorArticleCollectionViewCell
        cell.lTitle.text = self.articles[indexPath.row].title!
        cell.lTitle.lineBreakMode = .byWordWrapping
        cell.lTitle.numberOfLines = 0
        
        cell.lAuthorName.text = self.author.name
        
        if let date = self.articles[indexPath.row].date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            let dateString = dateFormatter.string(from: date)
            
            cell.lDate.text = dateString
        }
        
        if let imageURL = self.author.imageURL {
            cell.imageAuthor.af_setImage(withURL: imageURL)
        }
        
        if let content = self.articles[indexPath.row].content {
            let fontHTML = "<span style=\'font-family: %Dosis-Book; font-size: 34\'></span>"
            var html = content.replacingOccurrences(of: "700", with: "\(self.view.frame.width)")
            html = fontHTML + html
            html = html.replacingOccurrences(of: "394", with: "\(self.view.frame.width / 1.77)")
            cell.webViewContent.delegate = self
            cell.webViewContent.loadHTMLString(html, baseURL: nil)
        }
        
        return cell
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        for cell in collectionView.visibleCells {
            (cell as! AuthorArticleCollectionViewCell).webViewContent.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('body')[0].style.fontFamily =\"Dosis-Book\"")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
