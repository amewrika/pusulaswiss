//
//  AuthorArticleCollectionViewCell.swift
//  PusulaSwiss
//
//  Created by Ömer Baş on 06/04/2017.
//  Copyright © 2017 Pusula Swiss. All rights reserved.
//

import UIKit

class AuthorArticleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageAuthor: UIImageView!
    @IBOutlet weak var lTitle: UILabel!
    @IBOutlet weak var lAuthorName: UILabel!
    @IBOutlet weak var lDate: UILabel!
    @IBOutlet weak var webViewContent: UIWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
